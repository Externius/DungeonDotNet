﻿namespace RDMG.Web.Models.Profile;

public class ProfileViewModel : EditViewModel
{
    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
}